from __future__ import unicode_literals

from flask import Flask, flash, redirect, url_for, render_template, request, Response
from app import app, db, lm, youtube
from flask_login import login_user, logout_user, current_user
from .forms import AddSongForm, RegisterForm, LoginForm
from .models import User, Song
from .decorators import login_required
from jinja_filters import split_equal
import random
from sqlalchemy.sql.expression import func
import pafy
import sys
from os.path import expanduser

"""
@app.route('/download/<url>', methods=['GET', 'POST'])
def download_youtube_mp32(url):
    with youtube_dl.YoutubeDL(app.config['YDL_OPTS']) as ydl:
        ydl.download([url])
        flash('Musique en cours de téléchargement')
        return redirect(url_for('index'))
"""
# Problème en production actuellement, le problème est que, pour faire court, le téléchargement ne se passe pas chez l'utilisateur
# mais SUR le serveur Heroku, du coup l'utilisateur initialise le téléchargement mais le téléchargement se fait chez heroku
# Idée de test : Créer une application minimale, ,juste avec la librairie pafy, essayer de download, et voir le résultat
# Puis essayer de download un quelconque fichier à travers urrlib ou quelque chose du genre et voir le résultat
# De la déterminer si le problème vietn de paffy ou d'heroku
@app.route('/download/<url>', methods=['GET', 'POST'])
def download_youtube_mp3(url):
    res = pafy.new(url)
    bestaudio = res.getbestaudio(preftype="m4a")
    if sys.platform == 'win32':
        bestaudio.download(filepath=expanduser("~/"), quiet=False)
        print("Download on windows on {0}".format(expanduser("~")))
    else:
        bestaudio.download(filepath=expanduser("~/"), quiet=False)
        print("Download on linux on {0}".format(expanduser("~")))
    flash('Musique en cours de téléchargement')
    return redirect(url_for('index'))

@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404

@app.errorhandler(403)
def page_forbidden(e):
    return render_template('403.html'), 403

@app.errorhandler(410)
def page_gone(e):
    return render_template('410.html'), 410

@app.errorhandler(500)
def page_internal_server_error(e):
    return render_template('500.html'), 500

@app.route('/')
def index():
    prefixes = ["OG", "Lil", "Young", "Thug", "Killa"]
    random_prefix = random.choice(prefixes)
    songs = Song.query.order_by(Song.id.desc()).limit(10)
    return render_template('index.html', random_prefix=random_prefix, songs=songs)

@lm.user_loader
def load_user(user_email):
    return User.query.filter_by(email=user_email).first()

@app.route('/register', methods=['GET', 'POST'])
def register():
    form = RegisterForm(request.form)
    if request.method == 'POST':
        user = User(form.nickname.data, form.password.data, form.email.data)
        exists = db.session.query(User).filter_by(email=form.email.data).scalar() is not None
        if exists:
            flash("User already registered with this email address")
        else:
            db.session.add(user)
            db.session.commit()
            flash('Merci de votre enregistrement !', 'success')
            return redirect(url_for('index'))
    return render_template('register.html', form=form)

@app.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm(request.form)
    if request.method ==  'POST':
        user = User.query.filter_by(email=form.email.data).first()
        if user is not None and user.validate_password(form.password.data):
            login_user(user)
            return redirect('/')
        else:
            flash("Error, please retry.")
    return render_template('login.html', form=form)

@app.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('index'))

@app.route('/addsong', methods=['GET', 'POST'])
@login_required
def add_song():
    form = AddSongForm(request.form)
    if request.method == 'POST':
        s = Song(split_equal(form.url.data))
        s.name = print_vid_name(s.url)
        s.user_id = current_user.id
        exists = db.session.query(Song).filter_by(name=s.name).first()
        if exists:
            flash("Song already in database")
        else:
            db.session.add(s)
            db.session.commit()
            flash('Musique ajoutée')
            return redirect(url_for('index'))
    return render_template("add_song.html", form=form)

@app.route('/removesong/<int:id>', methods=['GET', 'POST'])
def remove_song(id):
    #s = models.Song.query.get(id).first_or_404()
    s = Song.query.filter_by(id=id).first()
    db.session.delete(s)
    db.session.commit()
    flash('Post deleted')
    return redirect(url_for('index'))

# A corriger dans la mesure ou renvoie une erreur si le son n'existe pas
@app.route('/randomsong')
def random_url_song():
    return redirect("https://www.youtube.com/watch?v=" + Song.query.order_by(func.random()).first().url)

def print_vid_name(url):
    result = youtube.videos().list(id=url, part='snippet').execute()
    for res in result.get('items', []):
        return res['snippet']['title']

@app.route('/users-list')
def users_list():
    users = User.query.filter().all()
    return render_template('users_list.html', users=users)

@app.route('/user/<nickname>')
def user_per_nickname(nickname):
    user = User.query.filter_by(nickname=nickname).first()
    songs = user.songs.all()
    if user == None:
        flash('User %s not found' % nickname)
        return redirect(url_for('index'))
    return render_template('user.html', user=user, songs=songs)

@app.route('/user/<int:id>')
def user_per_id(id):
    user = User.query.filter_by(id=id).first()
    songs = user.songs.all()
    if user == None:
        flash('User %d not found' % id)
        return redirect(url_for('index'))
    return render_template('user.html', user=user, songs=songs)

@app.route('/follow/<nickname>')
@login_required
def follow(nickname):
    user = User.query.filter_by(nickname=nickname).first()
    if user is None:
        flash('Utilisateur %s introuvable' % nickname)
        return redirect(url_for('index'))
    if user == current_user:
        flash('Vous ne pouvez pas vous suivre vous même')
        return redirect(url_for('user_per_nickname', nickname=nickname))
    u = current_user.follow(user)
    if u is None:
        flash('Vous ne pouvez pas suivre ' + nickname)
        return redirect(url_for('user_per_nickname', nickname=nickname))
    db.session.add(u)
    db.session.commit()
    flash('Vous suivez ' + nickname, 'success')
    return redirect(url_for('user_per_nickname', nickname=nickname))

@app.route('/unfollow/<nickname>')
@login_required
def unfollow(nickname):
    user = User.query.filter_by(nickname=nickname).first()
    if user is None:
        flash('Utilisateur %s not introuvable' % nickname)
        return redirect(url_for('index'))
    if user == current_user:
        flash('Vous ne pouvez pas vous suivre vous même')
        return redirect(url_for('user_per_nickname', nickname=nickname))
    u = current_user.unfollow(user)
    if u is None:
        flash('Impossible d\'arrêter de suivre ' + nickname)
        return redirect(url_for('user_per_nickname', nickname=nickname))
    db.session.add(u)
    db.session.commit()
    flash('Vous avez arrêté de suivre ' + nickname, 'danger')
    return redirect(url_for('user_per_nickname', nickname=nickname))