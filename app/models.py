from app import db, bcrypt
from app import app
from flask_login import UserMixin
import datetime

followers = db.Table('followers',
    db.Column('follower_id', db.Integer, db.ForeignKey('users.id')),
    db.Column('followed_id', db.Integer, db.ForeignKey('users.id'))
)

class Role(db.Model):
    __tablename__ = 'roles'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), unique=True)
    users = db.relationship('User', backref='role', lazy='dynamic')

    def __repr__(self):
        return '<Role %r>' % self.name

class User(UserMixin, db.Model):
    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key=True)
    nickname = db.Column(db.String(64), nullable=False)
    password = db.Column(db.String)
    email = db.Column(db.String(64), nullable=True, unique=True)
    songs = db.relationship('Song', backref="author", lazy="dynamic") #backref='users'
    role_id = db.Column(db.Integer, db.ForeignKey('roles.id'))
    confirmed = db.Column(db.Boolean, default=False)
    followed = db.relationship(
        'User',
        secondary=followers,
        primaryjoin=(followers.c.follower_id == id),
        secondaryjoin=(followers.c.followed_id == id),
        backref=db.backref('followers', lazy='dynamic'),
        lazy='dynamic',
    )

    def __init__(self, nickname, password, email):
        self.nickname = nickname
        self.password = bcrypt.generate_password_hash(password).decode('utf-8')
        self.email = email

    def validate_password(self, input):
        if bcrypt.check_password_hash(self.password, input):
            return True

    @property
    def is_authenticated(self):
        return True

    @property
    def is_active(self):
        return True

    @property
    def is_anonymous(self):
        return True

    def get_id(self):
        return self.email

    def follow(self, user):
        if not self.is_following(user):
            self.followed.append(user)
            return self

    def unfollow(self, user):
        if self.is_following(user):
            self.followed.remove(user)
            return self

    def is_following(self, user):
        return self.followed.filter(
            followers.c.followed_id == user.id).count() > 0

    def followed_songs(self):
        return Song.query.join(
            followers, (followers.c.followed_id == Song.user_id)).filter(
            followers.c.follower_id == self.id).order_by(
                    Song.timestamp.desc())


class Song(db.Model):
    __tablename__ = 'songs'
    id = db.Column(db.Integer, primary_key=True)
    url = db.Column(db.String(512), nullable=False, unique=True)
    name = db.Column(db.String(256), nullable=False)
    timestamp = db.Column(db.DateTime)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id')) #users.id

    def __init__(self, url): #name,user_id
        self.url = url
        #self.name = name
        self.timestamp = datetime.datetime.now() #In [31]: print (time.strftime("%Y-%m-%d %H:%M"))
        #self.user_id = user_id

    def __repr__(self):
        return '<Musique : <%r>' % (self.name)