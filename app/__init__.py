from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_login import LoginManager
from flask_gravatar import Gravatar
from apiclient.discovery import build
from flask_bcrypt import Bcrypt
import jinja_filters
import os

app = Flask(__name__)
bcrypt = Bcrypt(app)
app.config.from_object(os.environ['APP_SETTINGS'])
db = SQLAlchemy(app)

youtube = build('youtube', 'v3', developerKey=app.config['YOUTUBE_DEVELOPER_KEY'])
gravatar = Gravatar(app, size=100, rating='g', default='retro', force_default=False, force_lower=False, use_ssl=False, base_url=None)

migrate = Migrate(app, db)
lm = LoginManager(app)
lm.init_app(app)
lm.login_view = 'index'

app.jinja_env.filters['split_equal'] = jinja_filters.split_equal
app.jinja_env.filters['split_string_after'] = jinja_filters.split_string_after
app.jinja_env.filters['posted_at'] = jinja_filters.posted_at

from app import views, models