from flask_wtf import Form, RecaptchaField
from wtforms import StringField, BooleanField, PasswordField, validators
from wtforms.validators import DataRequired, Length

class AddSongForm(Form):
    url = StringField('URL', validators=[DataRequired()], render_kw={"placeholder": "Entrez l'ID de l'url youtube exemple : 1aTtF7ksYMc"})

class RegisterForm(Form):
    nickname = StringField('Nickname', [validators.Length(min=4, max=25)])
    email = StringField('Email Address', [validators.Length(min=6, max=50)])
    password = PasswordField('New Password', [
        validators.DataRequired(),
        validators.EqualTo('confirm', message='Password must match')
    ])
    confirm = PasswordField('Repeat Password')
    recaptcha = RecaptchaField()


class LoginForm(Form):
    email = StringField('Email', [DataRequired(), Length(min=6, max=50)])
    password = PasswordField('Password', [DataRequired])
