import os

basedir = os.path.abspath(os.path.dirname(__file__))

class Config(object):
    DEBUG = False
    TESTING = False
    SECRET_KEY = os.environ['SECRET_KEY'] or '40ee10de18ed9c6b4188950adbc19501ec0f10747746af33'

    # email server
    MAIL_SERVER = 'smtp.googlemail.com'
    MAIL_PORT = 465
    MAIL_USE_TLS = False
    MAIL_USE_SSL = True
    MAIL_USERNAME = os.environ.get('MAIL_USERNAME')
    MAIL_PASSWORD = os.environ.get('MAIL_PASSWORD')
    TRAPSTORM_MAIL_SUBJECT_PREFIX = '[Trapstorm]'
    TRAPSTORM_MAIL_SENDER = 'Trapstorm Admin <trapstorm001@gmail.com>'


    # administrator list
    ADMINS = ['trapstorm001@gmail.com']

    SQLALCHEMY_DATABASE_URI = os.environ['DATABASE_URL']
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    YOUTUBE_DEVELOPER_KEY = 'AIzaSyBDd0gKDKLHgvePwGNdDXKnHGAuaF4A5UI'

    OAUTH_CREDENTIALS = {
        'facebook': {
            'consumer_key': '568961276629867',
            'consumer_secret': '29136a226e6b03d5ac81092999c558b8'
        }
    }

    RECAPTCHA_PUBLIC_KEY = '6Le2TRAUAAAAAE_5XXAJQipTEt3msxKXK2BtKOSX'
    RECAPTCHA_PRIVATE_KEY = '6Le2TRAUAAAAAIs3KfvZbFmwe9udobcbxloiQvui'

    DOWNLOAD_DIR = "/tmp/"

    YDL_OPTS = {
        'format': 'bestaudio/best',
        'postprocessors': [{
            'key': 'FFmpegExtractAudio',
            'preferredcodec': 'mp3',
            'preferredquality': '320',
            'outtmpl': DOWNLOAD_DIR + u'%(id)s.%(ext)s',
        }],
    }


class TestConfig(Config):
    DEBUG = True
    TESTING = True


class DevelopmentConfig(Config):
    DEBUG = True


class ProductionConfig(Config):
    DEBUG = False

