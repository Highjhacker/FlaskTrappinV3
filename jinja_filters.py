import arrow

def split_equal(string):
    return string[32:]

def split_string_after(string):
    if len(string) > 49:
        return string[:50] + '...'
    return string

# Arrow
def posted_at(string):
    return arrow.get(str(string)).humanize()

